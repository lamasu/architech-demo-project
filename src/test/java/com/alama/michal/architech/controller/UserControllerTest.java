package com.alama.michal.architech.controller;

import com.alama.michal.architech.dao.UserDao;
import com.alama.michal.architech.model.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class UserControllerTest {
    @Mock
    private UserDao userDao;
    @Mock BindingResult bindingResult;
    private UserController userController = new UserController();

    @Before
    public void before() {
        userController = new UserController();
        userController.setUserDao(userDao);
    }

    @Test
    public void crate() throws Exception {
        // given
        Model model = new ExtendedModelMap();

        // when
        String view = userController.crate(model);

        // then
        assertEquals("/user/form", view);
        assertNotNull(model.asMap().get("user"));
        assertTrue(model.asMap().get("user") instanceof User);
    }

    @Test
    public void saveWhenValidUser() throws Exception {
        // given
        Model model = new ExtendedModelMap();
        User user = new User();

        // when
        String view = userController.save(user, bindingResult, model);

        // then
        assertEquals("/user/form", view);
        assertNotNull(model.asMap().get("user"));
        assertTrue(model.asMap().get("user") instanceof User);
        assertNotNull(model.asMap().get("isSuccess"));
        assertTrue(model.asMap().get("isSuccess") instanceof Boolean);
        assertTrue((Boolean) model.asMap().get("isSuccess"));
    }

    @Test
    public void saveWhenInvalidUser() throws Exception {
        // given
        Model model = new ExtendedModelMap();
        User user = new User();
        when(bindingResult.hasErrors()).thenReturn(true);

        // when
        String view = userController.save(user, bindingResult, model);

        // then
        assertEquals("/user/form", view);
        assertNull(model.asMap().get("isSuccess"));
    }

    @Test
    public void saveWhenUserAlreadyExists() throws Exception {
        // given
        Model model = new ExtendedModelMap();
        User user = new User();
        user.setUsername("john");
        when(userDao.getByUsername(Mockito.anyString())).thenReturn(new User());
        when(bindingResult.hasErrors()).thenReturn(true);

        // when
        String view = userController.save(user, bindingResult, model);

        // then
        assertEquals("/user/form", view);
        assertNull(model.asMap().get("isSuccess"));
        verify(bindingResult, times(1)).addError(any(FieldError.class));
    }
}