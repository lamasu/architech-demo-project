package com.alama.michal.architech.controller;

import com.alama.michal.architech.dao.UserDao;
import com.alama.michal.architech.model.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class IndexControllerTest {
    @Mock
    private UserDao userDao;
    private IndexController indexController = new IndexController();

    @Before
    public void before() {
        indexController = new IndexController();
        indexController.setUserDao(userDao);
    }

    @Test
    public void indexWhenUserListInNull() {
        // given
        Model model = new ExtendedModelMap();

        // when
        String view = indexController.index(model);

        // then
        assertEquals("index", view);
    }

    @Test
    public void indexWhenUserListIsNotEmpty() {
        // given
        Model model = new ExtendedModelMap();
        when(userDao.findAll()).thenReturn(Arrays.asList(new User()));

        // when
        String view = indexController.index(model);

        // then
        assertEquals("index", view);
        assertNotNull(model.asMap().get("users"));
        assertTrue(model.asMap().get("users") instanceof List);
        List<User> users = (List<User>) model.asMap().get("users");
        assertEquals(1, users.size());
    }
}