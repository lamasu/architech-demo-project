package com.alama.michal.architech.dao;

import com.alama.michal.architech.model.User;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class UserDaoImplTest {
    private UserDaoImpl userDao;
    @Mock
    private SessionFactory sessionFactory;
    @Mock
    private Session session;
    @Mock
    private Criteria criteria;
    @Mock
    private Transaction transaction;

    @Before
    public void before() {
        userDao = new UserDaoImpl();
        userDao.setSessionFactory(sessionFactory);
        when(sessionFactory.openSession()).thenReturn(session);
        when(session.getTransaction()).thenReturn(transaction);
        when(session.beginTransaction()).thenReturn(transaction);
    }
    @Test
    public void getByUsernameWhenDoesNotExist() throws Exception {
        // given
        String username = "michal";
        when(session.createCriteria(User.class)).thenReturn(criteria);

        // when
        User user = userDao.getByUsername(username);

        // then
        assertNull(user);
    }

    @Test
    public void getByUsernameWhenExists() throws Exception {
        // given
        String username = "michal";
        User existingUser = new User();
        existingUser.setUsername(username);
        when(session.createCriteria(User.class)).thenReturn(criteria);
        when(criteria.uniqueResult()).thenReturn(existingUser);

        // when
        User user = userDao.getByUsername(username);

        // then
        assertNotNull(user);
        assertEquals(username, user.getUsername());
    }

    @Test
    public void save() throws Exception {
        // given
        User user = new User();
        user.setUsername("michal");
        user.setPassword("asdasd45345SDFS");
        InOrder inOrder = inOrder(session, transaction);

        // when
        userDao.save(user);

        // then
        verify(session, times(1)).beginTransaction();
        verify(session, times(1)).getTransaction();
        verify(session, times(1)).save(user);
        verify(transaction, times(1)).commit();

        inOrder.verify(session).beginTransaction();
        inOrder.verify(session).save(user);
        inOrder.verify(session).getTransaction();
        inOrder.verify(transaction).commit();
    }

    @Test
    public void findAllWhenUsersListIsNull() throws Exception {
        // given
        when(session.createCriteria(User.class)).thenReturn(criteria);
        when(criteria.list()).thenReturn(null);

        // when
        List<User> users = userDao.findAll();

        // then
        assertNull(users);
    }

    @Test
    public void findAllWhenUsersListIsEmpty() throws Exception {
        // given
        when(session.createCriteria(User.class)).thenReturn(criteria);
        when(criteria.list()).thenReturn(Collections.EMPTY_LIST);

        // when
        List<User> users = userDao.findAll();

        // then
        assertNotNull(users);
        assertTrue(users.isEmpty());
    }

    @Test
    public void findAllWhenUserExists() throws Exception {
        // given
        when(session.createCriteria(User.class)).thenReturn(criteria);
        when(criteria.list()).thenReturn(Arrays.asList(new User(), new User()));

        // when
        List<User> users = userDao.findAll();

        // then
        assertNotNull(users);
        assertEquals(2, users.size());
    }
}