package com.alama.michal.architech.model;

import org.junit.BeforeClass;
import org.junit.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

public class UserTest {
    private static Validator validator;

    @BeforeClass
    public static void setUp() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    @Test
    public void validateTooShortName() {
        // given
        User user = new User();
        user.setUsername("abc");
        user.setPassword("Aa6yytggsmmms");

        // when
        Set<ConstraintViolation<User>> constraintViolations =  validator.validate(user);

        // then
        assertNotNull(constraintViolations);
        assertEquals(1, constraintViolations.size());
        assertEquals("Username must have minimum 5 characters.", constraintViolations.iterator().next().getMessage());
    }

    @Test
    public void validateExistingNotAlphanumericCharacters() {
        // given
        User user = new User();
        user.setUsername("abc!!!$$dfdf");
        user.setPassword("Aa6yytggsmmms");

        // when
        Set<ConstraintViolation<User>> constraintViolations =  validator.validate(user);

        // then
        assertNotNull(constraintViolations);
        assertEquals(1, constraintViolations.size());
        assertEquals("Username must be alphanumeric characters.", constraintViolations.iterator().next().getMessage());
    }

    @Test
    public void validateTooShortPassword() {
        // given
        User user = new User();
        user.setPassword("Aa6");
        user.setUsername("michal");

        // when
        Set<ConstraintViolation<User>> constraintViolations =  validator.validate(user);

        // then
        assertNotNull(constraintViolations);
        assertEquals(1, constraintViolations.size());
        assertEquals("Password must have minimum 8 characters.", constraintViolations.iterator().next().getMessage());
    }

    @Test
    public void validateMissingUppercaseInPassword() {
        // given
        User user = new User();
        user.setPassword("asdas453dfsdfs");
        user.setUsername("michal");

        // when
        Set<ConstraintViolation<User>> constraintViolations =  validator.validate(user);

        // then
        assertNotNull(constraintViolations);
        assertEquals(1, constraintViolations.size());
        assertEquals("Password must contain one uppercase character.", constraintViolations.iterator().next().getMessage());
    }

    @Test
    public void validateMissingLowercaseInPassword() {
        // given
        User user = new User();
        user.setPassword("ASDASD453FGDFGDF");
        user.setUsername("michal");

        // when
        Set<ConstraintViolation<User>> constraintViolations =  validator.validate(user);

        // then
        assertNotNull(constraintViolations);
        assertEquals(1, constraintViolations.size());
        assertEquals("Password must contain one lowercase character.", constraintViolations.iterator().next().getMessage());
    }

    @Test
    public void validateMissingDigitInPassword() {
        // given
        User user = new User();
        user.setPassword("abcdABCD");
        user.setUsername("michal");

        // when
        Set<ConstraintViolation<User>> constraintViolations =  validator.validate(user);

        // then
        assertNotNull(constraintViolations);
        assertEquals(1, constraintViolations.size());
        assertEquals("Password must contain one digit.", constraintViolations.iterator().next().getMessage());
    }

    @Test
    public void validateNullUsername() {
        // given
        User user = new User();
        user.setPassword("ALAULA12alaula");

        // when
        Set<ConstraintViolation<User>> constraintViolations =  validator.validate(user);

        // then
        assertNotNull(constraintViolations);
        assertEquals(1, constraintViolations.size());
        assertEquals("Username can not be null.", constraintViolations.iterator().next().getMessage());
    }

    @Test
    public void validateNullPassword() {
        // given
        User user = new User();
        user.setUsername("johnable");

        // when
        Set<ConstraintViolation<User>> constraintViolations =  validator.validate(user);

        // then
        assertNotNull(constraintViolations);
        assertEquals(1, constraintViolations.size());
        assertEquals("Password can not be null.", constraintViolations.iterator().next().getMessage());
    }

    @Test
    public void validateEmptyAttributes() {
        // given
        User user = new User();
        user.setUsername("");
        user.setPassword("");

        // when
        Set<ConstraintViolation<User>> constraintViolations =  validator.validate(user);

        // then
        assertNotNull(constraintViolations);

        List<String> messages = constraintViolations
                .stream()
                .map(u -> u.getMessage())
                .collect(Collectors.toList());

        assertEquals(5, messages.size());
        assertTrue(messages.contains("Password must have minimum 8 characters."));
        assertTrue(messages.contains("Password must contain one uppercase character."));
        assertTrue(messages.contains("Password must contain one digit."));
        assertTrue(messages.contains("Password must contain one lowercase character."));
        assertTrue(messages.contains("Username must have minimum 5 characters."));
    }
}