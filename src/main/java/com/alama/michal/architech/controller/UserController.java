package com.alama.michal.architech.controller;

import com.alama.michal.architech.dao.UserDao;
import com.alama.michal.architech.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;

@Controller
@RequestMapping("/user")
public class UserController {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserDao userDao;

    @RequestMapping(value="/crate", method= RequestMethod.GET)
    public String crate(Model model){
        LOGGER.info("Crate user page requested - GET");
        model.addAttribute("user", new User());
        return "/user/form";
    }

    @RequestMapping(value="/crate", method= RequestMethod.POST)
    public String save(@Valid User user, BindingResult bindingResult, Model model){
        LOGGER.info("Crate user page requested - POST");
        validateAgainstExistingUser(user, bindingResult);
        if (bindingResult.hasErrors()) {
            LOGGER.info("Validation errors occurred during adding user: {}", user.getUsername());
            return "/user/form";
        }

        userDao.save(user);
        model.addAttribute("isSuccess", true);
        LOGGER.info("User {} successfully registered", user.getUsername());

        return crate(model);
    }

    private void validateAgainstExistingUser(User user, BindingResult bindingResult) {
        String username = user.getUsername();
        if (!StringUtils.isEmpty(username) && userExists(username)) {
            bindingResult.addError(buildExistingUserError(username));
        }
    }

    private boolean userExists(String username) {
        User user = userDao.getByUsername(username);
        return user != null;
    }

    private FieldError buildExistingUserError(String username) {
        return new FieldError("username", "username", username, true, null, null, "Username already registered.");
    }

    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }
}
