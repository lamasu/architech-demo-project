package com.alama.michal.architech.controller;

import com.alama.michal.architech.dao.UserDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class IndexController {
    private static final Logger LOGGER = LoggerFactory.getLogger(IndexController.class);

    @Autowired
    private UserDao userDao;

    @RequestMapping(value={"/", "home", "index"}, method= RequestMethod.GET)
    public String index(Model model){
        LOGGER.info("Index page requested");
        model.addAttribute("users", userDao.findAll());
        return "index";
    }

    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }
}