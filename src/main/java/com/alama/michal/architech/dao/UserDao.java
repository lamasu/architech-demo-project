package com.alama.michal.architech.dao;

import com.alama.michal.architech.model.User;

import java.util.List;

public interface UserDao {
    User getByUsername(String username);
    void save(User user);
    List<User> findAll();
}
