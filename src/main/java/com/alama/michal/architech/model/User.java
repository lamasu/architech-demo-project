package com.alama.michal.architech.model;

import org.hibernate.validator.constraints.Length;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Entity
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Length(min = 5, message = "Username must have minimum 5 characters.")
    @Pattern(regexp = "^[A-Za-z0-9]*$", message = "Username must be alphanumeric characters.")
    @NotNull(message = "Username can not be null.")
    private String username;

    @Length(min = 8, message = "Password must have minimum 8 characters.")
    @Pattern.List({
            @Pattern(regexp = "(?=.*[0-9]).+", message = "Password must contain one digit."),
            @Pattern(regexp = "(?=.*[A-Z]).+", message = "Password must contain one uppercase character."),
            @Pattern(regexp = "(?=.*[a-z]).+", message = "Password must contain one lowercase character."),

    })
    @NotNull(message = "Password can not be null.")
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
